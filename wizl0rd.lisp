;;;; wizl0rd.lisp

(in-package :wizl0rd)

(defparameter *wizl0rd-copyright-notice*
"                         ###                 
#    # # ###### #       #   #  #####  #####  
#    # #     #  #      #   # # #    # #    # 
#    # #    #   #      #  #  # #    # #    # 
# ## # #   #    #      # #   # #####  #    # 
##  ## #  #     #       #   #  #   #  #    # 
#    # # ###### ######   ###   #    # #####  
-----------------------------------------------------------------
Welcome to wizl0rd. Copyright (C) 2006-2016 by David T. O'Toole 
email: <dto@xelf.me>   website: http://xelf.me/
This game uses Creative Commons art from http://opengameart.org
Character art by Charles Gabriel. Terrain and UI art by Denzi.
Castle walls and additional art by Andre Mari Coppola.
See the included files ./opengameart/OGA-*-LICENSE.txt files for 
complete OpenGameArt licensing information.

Wizl0rd is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Wizl0rd is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. Full license text of the GNU Lesser
General Public License is in the enclosed file named 'COPYING'. Full
license texts for compilers, assets, libraries, and other items are
contained in the LICENSES directory included with this application.
-----------------------------------------------------------------
")

(defun image-set (name count &optional (start 1))
  (loop for n from start to count
	collect (format nil "~A-~S.png" name n)))

;;; Character and Object Sprite Sheets

(defparameter *default-frame-delay* 8)
(defparameter *character-scale* 3)
(defparameter *character-cell-width* 32) 
(defparameter *character-cell-height* 40) 
(defparameter *cell-width* 32)
(defparameter *cell-height* 32)
(defparameter *width* 1280)
(defparameter *height* 720)

(defparameter *character-tiles-file* "character-tiles.png")
(defresource "character-tiles.png"
  :tile-height 60
  :tile-width 54)

(defparameter *medieval-tiles-file* "medieval-tiles.png")
(defresource "medieval-tiles.png"
  :tile-height 48
  :tile-width 48)
(defparameter *other-tiles-file* "other-tiles.png")

(defparameter *effects-tiles-file* "effects.png")
(defresource "effects.png"
  :tile-height 48
  :tile-width 48)

(defparameter *spritesheet-animations* nil)

(defun spritesheet-animation-parameters (animation)
  (getf *spritesheet-animations* animation))

(defun spritesheet-tile-width (spritesheet)
  (find-resource-property spritesheet :tile-width))

(defun spritesheet-tile-height (spritesheet)
  (find-resource-property spritesheet :tile-height))

(defun spritesheet-width (spritesheet)
  (image-width spritesheet))

(defun spritesheet-height (spritesheet)
  (image-height spritesheet))

(defparameter *character-block-width* 12)
(defparameter *character-block-height* 3)
(defparameter *character-row-offset* 1)

;;; Character jobs

(defparameter *character-jobs*
  '(:warrior :wizard :healer :ninja :ranger :town-3 :monk :berserker :dark-knight
    :soldier :town-1 :town-2 :priest :pirate :captain :samurai :lord :merchant
    :bunny :bard :dancer :elderly :child :vampire :paladin :cultist :fire :ice
    :earth :wind :light :dark))

(defparameter *female-only-jobs* '(:bunny :bard :dancer))
(defparameter *male-only-jobs* '(:pirate :captain :merchant :vampire :paladin))
(defparameter *elemental-jobs* '(:fire :ice :earth :wind :light :dark))
(defparameter *undead-jobs* '(:dark-knight :vampire :cultist))

(defparameter *human-jobs*
  (set-difference *character-jobs* *elemental-jobs*))

(defparameter *unisex-jobs*
  (set-difference *human-jobs* (append *male-only-jobs* *female-only-jobs*)))

(defun random-sex () (random-choose '(:male :female)))
(defun random-sex-for-job (job)
  (cond ((male-only-job-p job) :male)
	((female-only-job-p job) :female)
	((unisex-job-p job) (random-sex))
	(t (random-sex))))

;;; Job predicates

(defun human-job-p (job) (member job *human-jobs*))
(defun elemental-job-p (job) (member job *elemental-jobs*))
(defun undead-job-p (job) (member job *undead-jobs*))

(defun male-only-job-p (job) (member job *male-only-jobs*))
(defun female-only-job-p (job) (member job *female-only-jobs*))
(defun unisex-job-p (job) (member job *unisex-jobs*)) 

(defun male-job-p (job)
  (and (human-job-p job)
       (not (female-only-job-p job))))

(defun female-job-p (job)
  (and (human-job-p job)
       (not (male-only-job-p job))))

;; Townspeople, elderly, and children cannot be killed or fight in
;; this game.

(defparameter *innocent-jobs* '(:child :elderly :town-1 :town-2 :town-3))
(defparameter *combat-jobs* (set-difference *human-jobs* *innocent-jobs*))
(defun innocent-job-p (job) (member job *innocent-jobs*))
(defun combat-job-p (job) (member job *combat-jobs*))

;; Currently cannot play as Innocent, Elemental, or Undead

(defparameter *player-jobs* (set-difference *combat-jobs* *undead-jobs*))
(defun player-job-p (job) (member job *player-jobs*))

;;; Animations

(defparameter *character-animations* 
  '(:walk-up (:row 0 :start 0 :end 2)
    :walk-right (:row 1 :start 0 :end 2)
    :walk-left (:row 1 :start 0 :end 2 :mirror :horizontal)
    :walk-down (:row 2 :start 0 :end 2)
    ;;
    :attack-up (:row 0 :start 3 :end 6)
    :attack-right (:row 1 :start 3 :end 6)
    :attack-left (:row 1 :start 3 :end 6 :mirror :horizontal)
    :attack-down (:row 2 :start 3 :end 6)
    ;;
    :cast-up (:row 0 :start 7 :end 11)
    :cast-right (:row 1 :start 7 :end 11)
    :cast-left (:row 1 :start 7 :end 11 :mirror :horizontal)
    :cast-down (:row 2 :start 7 :end 11)))

(defparameter *character-bases*
  '(:warrior (:row 4 :male 0 :female 12)
    :wizard (:row 7 :male 0 :female 12)
    :healer (:row 10 :male 0 :female 12)
    :ninja (:row 13 :male 0 :female 12)
    :ranger (:row 16 :male 0 :female 12)
    :town-3 (:row 19 :male 0 :female 12)
    :monk (:row 22 :male 0 :female 12)
    :berserker (:row 25 :male 0 :female 12)
    :dark-knight (:row 28 :male 0 :female 12)
    :soldier (:row 31 :male 0 :female 12)
    :town-1 (:row 34 :male 0 :female 12)
    :town-2 (:row 37 :male 0 :female 12)
    :priest (:row 40 :male 0 :female 12)
    :pirate (:row 43 :male 0)
    :captain (:row 43 :male 12)
    :samurai (:row 46 :male 0 :female 12)
    :lord (:row 49 :male 0 :female 12)
    :merchant (:row 52 :male 0)
    :bunny (:row 52 :female 12) 
    :bard (:row 55 :female 0)
    :dancer (:row 55 :female 12)
    :elderly (:row 58 :male 0 :female 12)
    :child (:row 61 :male 0 :female 12)
    :vampire (:row 64 :male 0)
    :paladin (:row 64 :male 12)
    :cultist (:row 67 :male 0 :female 12)
    :fire (:row 70 :male 0)
    :ice (:row 70 :male 12)
    :earth (:row 73 :male 0)
    :wind (:row 73 :male 12)
    :light (:row 76 :male 0)
    :dark (:row 76 :male 12)))

(defun character-base (job)
  (getf *character-bases* job))

(defun character-base-row (job)
  (let ((entry (character-base job)))
    (when entry
      (- (getf entry :row)
	 *character-row-offset*))))

(defun character-base-column (job &optional (sex :male))
  (let ((entry (character-base job)))
    (when entry
      (getf entry sex))))

(defun character-animation-parameters (animation)
  (getf *character-animations* animation))

(defun character-animation-frames-* (animation job &optional (sex :male))
  (let ((parameters (character-animation-parameters animation)))
    (when parameters
      (destructuring-bind (&key row start end mirror
				(delay *default-frame-delay*)
			   &allow-other-keys) parameters
	(let ((base-row (character-base-row job))
	      (base-column (character-base-column job sex)))
	  (loop for x from start
		collect (list
			 *character-tiles-file*
			 (+ row base-row)
			 (+ x base-column)
			 mirror
			 delay)
	      while (< x end)))))))

(defun-memo character-animation-frames (animation job sex)
    (:key #'identity :test #'equal)
  (character-animation-frames-* animation job sex))

(defun spritesheet-animation-frames-* (spritesheet parameters)
  (destructuring-bind (&key row start end mirror
			    (delay *default-frame-delay*)
		       &allow-other-keys) parameters
    (loop for column from start
	  collect (list spritesheet row column mirror delay)
	  while (< column end))))

(defun-memo spritesheet-animation-frames (spritesheet parameters)
    (:key #'identity :test #'equal)
  (spritesheet-animation-frames-* spritesheet parameters))

(defun single-frame (spritesheet row column)
  (list spritesheet row column))

(defun collect-frames (spritesheet row start end)
  (loop for column from start
	collect (single-frame spritesheet row column)
	while (< column end)))

(defparameter *tree-tiles* (collect-frames *medieval-tiles-file* 0 4 6))
(defparameter *house-tiles* (collect-frames *medieval-tiles-file* 1 0 2))
(defparameter *mountain-tiles* (collect-frames *medieval-tiles-file* 1 3 5))
(defparameter *tower-tile* (single-frame *medieval-tiles-file* 3 2))
(defparameter *garrison-tile* (single-frame *medieval-tiles-file* 4 6))
(defparameter *player-flag* (single-frame *medieval-tiles-file* 6 2))
(defparameter *enemy-flag* (single-frame *medieval-tiles-file* 6 5))
(defparameter *wall-tee-left* (single-frame *medieval-tiles-file* 31 0))
(defparameter *wall-tee-right* (single-frame *medieval-tiles-file* 31 1))
(defparameter *wall-tee-down* (single-frame *medieval-tiles-file* 32 0))
(defparameter *wall-tee-up* (single-frame *medieval-tiles-file* 32 1))
(defparameter *wall-corner-upleft* (single-frame *medieval-tiles-file* 31 2))
(defparameter *wall-corner-downleft* (single-frame *medieval-tiles-file* 33 2))
(defparameter *wall-corner-upright* (single-frame *medieval-tiles-file* 31 4))
(defparameter *wall-corner-downright* (single-frame *medieval-tiles-file* 33 4))
(defparameter *wall-left* (single-frame *medieval-tiles-file* 31 3))
(defparameter *wall-right* (single-frame *medieval-tiles-file* 33 3))
(defparameter *wall-down* (single-frame *medieval-tiles-file* 32 2))
(defparameter *wall-up* (single-frame *medieval-tiles-file* 32 4))
(defparameter *wall-tower-right* (single-frame *medieval-tiles-file* 4 1))
(defparameter *wall-tower-up* (single-frame *medieval-tiles-file* 31 5))
(defparameter *skull-tile* (single-frame *medieval-tiles-file* 43 0))

(defparameter *missile-bases*
  '(:poison (:row 3 :column 12)
    :fire (:row 12 :column 12)
    :needle (:row 21 :column 12)
    :light (:row 3 :column 16)
    :ice (:row 12 :column 16)
    :crystal (:row 21 :column 16)
    :chaos (:row 3 :column 21)
    :lightning (:row 12 :column 21)
    :magma (:row 21 :column 21)
    :holy (:row 3 :column 26)))

(defun missile-facing-offset (facing)
  (position facing '(:right :upright :up :upleft :left :downleft :down :downright)))

(defun missile-animation (type facing)
  (let ((parameters (getf *missile-bases* type)))
    (destructuring-bind (&key row column) parameters
      (list :row (+ row (missile-facing-offset facing))
	    :start column
	    :end (+ column 2)))))

(defun effect-animation (effect)
  (ecase effect
    (:sparkle '(:row 3 :start 0 :end 2))
    (:fire '(:row 12 :start 0 :end 2))
    (:ice '(:row 13 :start 0 :end 2))
    (:poison '(:row 14 :start 0 :end 2))
    (:dark '(:row 15 :start 0 :end 2))
    (:chaos '(:row 16 :start 0 :end 2))
    (:explosion '(:row 23 :start 0 :end 2))
    (:summon '(:row 24 :start 0 :end 2))))

(defun missile-animation-frames (type facing)
  (spritesheet-animation-frames *effects-tiles-file* (missile-animation type facing)))

(defun effect-animation-frames (effect)
  (spritesheet-animation-frames *effects-tiles-file* (effect-animation effect)))

(defun frame-bounding-box-* (frame)
  (destructuring-bind (spritesheet row column &optional mirror delay) frame
    (let* ((tile-height (spritesheet-tile-height spritesheet))
	   (tile-width (spritesheet-tile-width spritesheet))
	   (file-height (spritesheet-height spritesheet))
	   (file-width (spritesheet-width spritesheet))
	   (top (* row tile-height))
	   (left (* column tile-width))
	   (right (+ left tile-width))
	   (bottom (+ top tile-height)))
      (when (eq mirror :horizontal)
	(rotatef left right))
      (list
       (/ (+ top 0.5) file-height)
       (/ (+ left 0.5) file-width)
       (/ (- right 0.5) file-width)
       (/ (- bottom 0.5) file-height)
       ))))

(defun-memo frame-bounding-box (frame)
    (:key #'identity :test #'equal)
  (frame-bounding-box-* frame))

(defun-memo character-frame-bounding-box (frame)
    (:key #'identity :test #'equal)
  (frame-bounding-box-* frame))

(defun frame-delay (frame)
  (fifth frame))

(defparameter *character-height-overspill* (- *character-cell-height* *character-cell-width*))

(defun draw-frame
    (frame x y &optional (width *cell-width*)
			 (height *cell-height*))
  (destructuring-bind (v1 u1 u2 v2)
      (character-frame-bounding-box frame)
    (let ((left x)
	  (right (+ x width))
	  (top y)
	  (bottom (+ y height))
	  (spritesheet (first frame)))
      (xelf::enable-texture-blending)	
      (set-blending-mode :alpha)
      (gl:bind-texture :texture-2d (find-texture spritesheet))
      (gl:matrix-mode :modelview)
      (xelf::set-vertex-color "white")
      (gl:with-pushed-matrix 
	(gl:load-identity)
	(gl:with-primitive :quads
	  (gl:tex-coord u1 v2)
	  (gl:vertex x bottom 0)
	  (gl:tex-coord u2 v2)
	  (gl:vertex right bottom 0)
	  (gl:tex-coord u2 v1)
	  (gl:vertex right y 0)
	  (gl:tex-coord u1 v1)
	  (gl:vertex x y 0))))))
			       
(defparameter *paused* nil)
(defparameter *soundtrack* nil)
(defparameter *grid-directions* '(:right :up :left :down))
(defparameter *grid-diagonals* '(:upright :upleft :downright :downleft))

;;; Game clock and scoring

(defparameter *score-font* "sans-mono-bold-12")
(defparameter *big-font* "sans-mono-bold-16")

(defun seconds (n) (* 60 n))
(defun minutes (n) (* (seconds 60) n))

(defparameter *game-length* (minutes 3))

(defvar *game-clock* 0)

(defun reset-game-clock ()
  (setf *game-clock* 0))

(defun update-game-clock ()
  (incf *game-clock*))

(defun game-on-p () (plusp *game-clock*))

(defun game-clock () *game-clock*)

(defun game-clock-string (&optional (clock *game-clock*))
  (let ((minutes (/ clock (minutes 1)))
	(seconds (/ (rem clock (minutes 1)) 
		    (seconds 1))))
    (format nil "~D~A~2,'0D" (truncate minutes) ":" (truncate seconds))))

(defvar *score-1* 0)
(defvar *score-2* 0)

(defun reset-score () (setf *score-1* 0 *score-2* 0))

;;; A grid of objects

(setf xelf:*unit* *character-cell-width*)

(defmethod grid-offset ((buffer buffer)) 0)
(defmethod grid-object-size ((buffer buffer)) *character-cell-width*)

(defun grid-position (x y)
  (values (units x) (units y)))

(defun grid-position-center (x y)
  (values (cfloat (+ (units x) (units 0.5)))
	  (cfloat (+ (units y) (units 0.5)))))

(defun valid-position-p (x y)
  (and (not (minusp x))
       (not (minusp y))
       (< x (width-in-units))
       (< y (height-in-units))))

(defun grid-position-bounding-box (x y)
  ;; top left right bottom
  (values (units y) 
	  (units x) 
	  (units (1+ x)) 
	  (units (1+ y))))

(defparameter *margin-width* 0.5)

(defun grid-object-bounding-box (x y)
  (values (+ (units y) *margin-width*)
	  (+ (units x) *margin-width*)
	  (- (units (1+ x)) *margin-width*)
	  (- (units (1+ y)) *margin-width*)))

;;; Uniformly sized grid objects

(defparameter *slide-frames* 45)

(defparameter *default-action-points* 0)
(defparameter *default-maximum-health* 100)

(defclass thing (xelf:node)
  ((grid-x :initform 0 :accessor grid-x :initarg :grid-x)
   (grid-y :initform 0 :accessor grid-y :initarg :grid-y)
   (obstacle-p :initform nil :accessor obstacle-p :initarg :obstacle-p)
   (color :initform "white" :accessor color :initarg :color)
   (direction :initform :down :accessor direction :initarg :direction)
   ;; slide movement
   (gx0 :initform 0)
   (gy0 :initform 0)
   (px :initform 0)
   (py :initform 0)
   (pz :initform 0)
   (x0 :initform 0)
   (y0 :initform 0)
   (z0 :initform 0)
   (origin-gx :initform 0)
   (origin-gy :initform 0)
   (interpolation :initform :linear)
   (frames :initform 0)
   (slide-frames :initform *slide-frames*)
   (slide-timer :initform 0)
   (delay :initform 0)
   (animation-frames :initform nil :accessor animation-frames)
   (image :initform nil)
   (static-frame :initform nil :accessor static-frame :initarg :static-frame)
    ;; player attributes
   (maximum-health :initform *default-maximum-health* :initarg :maximum-health :accessor maximum-health)
   (health-points :initform *default-maximum-health* :initarg :health-points :accessor health-points)
   (action-points :initform *default-action-points* :initarg :action-points :accessor action-points)
   (hunger :initform 0 :accessor hunger)
   (cold :initform 0 :accessor cold)
   ;; modifiers
   (attack-rating :initform 0 :accessor attack-rating)
   (defense-rating :initform 0 :accessor defense-rating)
   (resistance-rating :initform 0 :accessor resistance-rating)
   ;; time stop and other conditions
   (stasis :initform nil)
   (bleeding :initform nil)))

(defmethod grid-coordinates ((thing thing))
  (values (grid-x thing) (grid-y thing)))

;;; Derived attack/defense/resistance scores 

(defun rating-slot-name (stat)
  (ecase stat
    (:attack 'attack-rating)
    (:defense 'defense-rating)
    (:resistance 'resistance-rating)))

(defun stat-slot-name (stat)
  (ecase stat
    (:health 'health-points)
    (:action 'action-points)))

(defmethod compute-rating ((self thing) stat)
  (slot-value self (rating-slot-name stat)))
  ;; (apply #'+ 
  ;; 	 (field-value stat self) 
  ;; 	 (mapcar #'(lambda (item)
  ;; 		     (field-value stat item))
  ;; 		 (equipment self))))

(defconstant +rating-unit-percentage-points+ 10)

(defmethod compute-modifier ((self thing) stat)
 (cfloat
   (/ (+ 100 (* (compute-rating self stat)
		+rating-unit-percentage-points+))
      100)))

(defmethod attack-modifier ((self thing))
  (compute-modifier self :attack))

(defmethod defense-modifier ((self thing))
  (- 1 (- (compute-modifier self :defense) 1)))

(defmethod resistance-modifier ((self thing))
  (- 1 (- (compute-modifier self :resistance) 1)))

;;; Vital attributes

(defparameter *maximum-points* 100)

(defmethod modify-points ((self thing) stat points)
  (setf (slot-value self (stat-slot-name stat))
	(truncate 
	 (max 0
	      (min *maximum-points*
		   (+ points 
		      (slot-value self (stat-slot-name stat))))))))

(defmethod modify-health ((self thing) points)
  (modify-points self :health points))

(defmethod damage ((self thing) points)
  (modify-health self (* points (defense-modifier self))))
		 
(defmethod modify-health :after ((self thing) points)
  (when (not (plusp (slot-value self 'health-points)))
    (kill self)))

(defmethod modify-action-points ((self thing) points)
  (modify-points self :action points))

(defmethod modify-hunger ((self thing) points)
  (modify-points self :hunger points))

;;; Grid placement

(defmethod find-special ((thing thing)) nil)

(defmethod resize-to-grid ((thing thing))
  (resize thing (grid-object-size (current-buffer)) (grid-object-size (current-buffer))))

(defmethod initialize-instance :after ((thing thing) &key)
  (resize-to-grid thing))

(defmethod move-to-grid ((thing thing) gx gy)
  (assert (and (integerp gx) (integerp gy)))
  (with-slots (height width grid-x grid-y gx0 gy0) thing
    (multiple-value-bind (x y) (grid-position gx gy)
      (setf grid-x gx grid-y gy)
      (move-to thing 
	       (+ x (grid-offset (current-buffer)))
	       (+ y (grid-offset (current-buffer)))
	       (setf gx0 gx gy0 gy)))))

(defun place (thing x y)
  (with-buffer (current-buffer)
    (add-node (current-buffer) thing)
    (move-to-grid thing x y)))

;;; Sliding movement 

(defmethod sliding-p ((thing thing))
  (plusp (slot-value thing 'slide-timer)))

(defun interpolate (px x a &optional (interpolation :linear))
  (let ((range (- px x)))
    (float 
     (+ px
	(ecase interpolation
	  (:linear (* a range))
	  (:sine (* (sin (* a (/ pi 2))) range)))))))

(defmethod slide-to ((self thing) x1 y1 &key (interpolation :linear) frames)
  (with-slots (x y px py x0 y0 interpolation slide-timer slide-frames) self
    (setf px x py y)
    (setf x0 x1 y0 y1)
    (setf interpolation interpolation)
    (let ((timer (or frames slide-frames)))
      (setf slide-timer timer frames timer))))

(defmethod slide-to-grid ((thing thing) gx gy &key (interpolation :linear) frames)
  (multiple-value-bind (cx cy) (grid-position-center gx gy)
    (with-slots (grid-x grid-y gx0 gy0 origin-gx origin-gy height width slide-frames) thing
      (setf gx0 gx gy0 gy)
      (setf origin-gx grid-x origin-gy grid-y) 
      (let ((timer (or frames slide-frames)))
	(slide-to thing (- cx (/ width 2)) (- cy (/ height 2)) :interpolation interpolation :frames timer)))))

(defmethod update-slide ((self thing))
  (with-slots (x0 y0 px py slide-frames slide-timer interpolation) self
    (unless (zerop slide-timer)
      ;; scale to [0,1]
      (let ((a (/ (- slide-timer slide-frames) slide-frames)))
	(move-to self
		 (interpolate px x0 a interpolation)
		 (interpolate py y0 a interpolation))
	(decf slide-timer)))))

(defmethod cancel-slide ((thing thing))
  (setf (slot-value thing 'slide-timer) 0))

(defmethod snap-to-grid ((thing thing))
  (with-slots (gx0 gy0) thing
    (move-to-grid thing gx0 gy0)))

(defmethod fuzz-to-grid ((thing thing))
  (with-slots (x y) thing
    (let ((tolerance 3.0)
	  (gx (round (/ x (units 1))))
	  (gy (round (/ y (units 1)))))
      (if (< tolerance (distance x y (* *unit* gx) (* *unit* gy)))
	  (message "Warning: could not fuzzy-match ~S for grid position ~S." thing (list gx gy 'from x y))
	  (move-to-grid thing gx gy)))))

(defmethod restore-from-slide ((thing thing))
  (with-slots (origin-gx origin-gy) thing
    (move-to-grid thing origin-gx origin-gy)
    (snap-to-grid thing)))

(defmethod think ((thing thing)) nil)

(defmethod should-think-p ((thing thing))
  (not (sliding-p thing)))

(defmethod arrive ((thing thing)) 
  (snap-to-grid thing)
  (when (should-think-p thing)
    (think thing)))

(defmethod update :before ((thing thing))
  (update-animation thing)
  (if (sliding-p thing)
      (progn (update-slide thing)
	     ;; did we arrive?
	     (when (not (sliding-p thing))
	       (with-slots (grid-x gx0 grid-y gy0) thing
		 (setf grid-x gx0 grid-y gy0)
		 (arrive thing))))
      (when (should-think-p thing)
	(think thing))))

(defmethod cursor ((thing thing)) (player-1))

(defmethod distance-to-cursor ((thing thing))
  (multiple-value-bind (cx cy) (center-point thing)
    (multiple-value-bind (px py) (center-point (cursor thing))
      (distance cx cy px py))))

(defmethod heading-to-cursor ((thing thing))
  (multiple-value-bind (cx cy) (center-point thing)
    (multiple-value-bind (px py) (center-point (cursor thing))
      (find-heading cx cy px py))))

(defmethod direction-to-cursor ((thing thing))
  (or (heading-direction (heading-to-cursor thing)) :right))

(defmethod update-heading ((thing thing))
  (with-slots (heading direction) thing
    (setf heading (direction-heading direction))))

(defmethod face ((thing thing) direction)
  (setf (direction thing) direction)
  (update-heading thing))

(defmethod facing ((thing thing))
  (slot-value thing 'direction))

(defmethod move-grid ((thing thing) direction &optional (distance 1))
  (with-slots (grid-x grid-y) thing
    (multiple-value-bind (x y) 
	(step-in-direction grid-x grid-y direction distance)
      (move-to-grid thing x y))))

(defmethod slide ((thing thing) direction &optional (distance 1) frames)
  (with-slots (grid-x grid-y slide-frames) thing
    (assert (integerp slide-frames))
    (assert (keywordp direction))
    (assert (integerp distance))
    (multiple-value-bind (x y) 
	(step-in-direction grid-x grid-y direction distance)
      (slide-to-grid thing x y :frames (or frames slide-frames)))))

(defmethod move-forward ((thing thing) &optional (distance 1))
  (with-slots (direction) thing
    (move-grid thing direction distance)))

(defmethod slide-forward ((thing thing) &optional (distance 1) frames)
  (with-slots (direction slide-frames) thing
    (assert (integerp slide-frames))
    (assert (keywordp direction))
    (assert (integerp distance))
    (slide thing direction distance slide-frames)))

(defmethod turn-around ((thing thing))
  (face thing (opposite-direction (direction thing))))

(defun leftward (direction) (xelf::left-turn (xelf::left-turn direction)))
(defun rightward (direction) (xelf::right-turn (xelf::right-turn direction)))

(defmethod turn-leftward ((thing thing))
  (face thing (leftward (direction thing))))

(defmethod turn-rightward ((thing thing))
  (face thing (rightward (direction thing))))

(defmethod find-color ((thing thing))
  (slot-value thing 'color))

(defmethod paint ((thing thing) color)
  (setf (slot-value thing 'color) color))

(defun same-color-p (a b)
  (string= (find-color a) (find-color b)))

(defmethod draw ((self thing))
  (with-slots (x y image color heading) self
    (if image
	(draw-textured-rectangle-* x y 0 *cell-width* *cell-height*
				   (find-texture image)
				   ;; apply shading
				   :vertex-color color
				   :blend :alpha
				   ;; adjust angle to normalize for up-pointing sprites 
				   :angle (+ 90 (heading-degrees heading)))
	(draw-frame
	 (or (current-animation-frame self)
	     (default-animation-frame self))
	 x y
	 *cell-width*
	 *cell-height*))))

(defmethod handle-collision ((u thing) (v thing))
  (collide u v)
  (collide v u))

(defmethod center-of-arena ()
  (values (/ *width* 2) (/ *height* 2)))

(defmethod heading-to-center ((thing thing))
  (multiple-value-bind (tx ty) (center-point thing)
    (multiple-value-bind (cx cy) (center-of-arena)
      (find-heading tx ty cx cy))))
  
(defun clamp (x bound)
  (max (- bound)
       (min x bound)))

(defun decay (x)
  (let ((z (* 0.94 x)))
    z))

;;; Running the animations

(defmethod current-animation-frame ((thing thing))
  (first (slot-value thing 'animation-frames)))

(defmethod default-animation-frame ((thing thing))
  (static-frame thing))

(defmethod begin-animation ((thing thing) frames)
  (with-slots (animation-frames delay) thing
    (setf animation-frames frames)
    (setf delay (frame-delay (first frames)))))

(defmethod update-animation ((thing thing))
  (with-slots (animation-frames delay) thing
    (when animation-frames
      (when (not (minusp delay))
	(decf delay))
      (when (minusp delay)
	(pop animation-frames)
	(if animation-frames 
	    (setf delay (frame-delay (first animation-frames)))
	    (setf animation-frames (list (default-animation-frame thing))
		  delay *default-frame-delay*))))))

(defmethod end-animation ((thing thing))
  (setf (animation-frames thing) nil))

(defmethod kill ((thing thing))
  (destroy thing))

;;; Job system

(defclass player (thing)
  ((character-name :initform "Unnamed player" :accessor character-name :initarg character-name)
   (job :initform :warrior :accessor job :initarg :job)
   (sex :initform :male :accessor sex :initarg :sex)
   (player-id :initform nil :accessor player-id :initarg :player-id)
   (formation-p :initform nil :accessor formation-p :initarg :formation-p)
   (leader :initform nil :accessor leader :initarg :leader)
   (followers :initform nil :accessor followers :initarg :followers)
   (input-direction :initform nil :accessor input-direction)
   (input-fire :initform nil :accessor input-fire)
   (input-deploy :initform nil :accessor input-deploy)
   (deadp :initform nil :accessor deadp)
   (slide-frames :initform *slide-frames*)
   (throw-timer :initform 0 :accessor throw-timer)))

(defmethod human-player-p ((player player))
  (numberp (player-id player)))

(defmethod formation-leader-p ((player player))
  (and (formation-p player)
       (followers player)))

(defmethod formation-follower-p ((player player))
  (and (formation-p player)
       (leader player)))

(defmethod party-leader-p ((player player)) (and (numberp (player-id player))
					       (= 1 (player-id player))))
(defmethod party-follower-p ((player player)) (and (numberp (player-id player))
						 (< 1 (player-id player))))
(defmethod lead-party ((player player)) (setf (player-id player) 1))
(defmethod follow-leader ((player player)) (setf (player-id player) 2))

(defun party-leader ()
  (find-if #'party-leader-p (find-instances (current-buffer) 'player)))

(defun party-followers ()
  (remove-if-not #'party-follower-p (find-instances (current-buffer) 'player)))

(defun party () (cons (party-leader) (party-followers)))

(defun rotate-party ()
  (let* ((leader (party-leader))
	 (follower (first (party-followers)))
	 (dir (facing leader)))
    (slide-forward follower)
    (face leader (opposite-direction (facing follower)))
    (slide-forward leader)
    (lead-party follower)
    (follow-leader leader)))

(defmethod innocent-p ((player player))
  (innocent-job-p (job player)))

(defmethod combatant-p ((player player))
  (combat-job-p (job player)))

(defmethod kill ((player player))
  (when (and (not (innocent-p player))
	     (not (deadp player)))
    (setf (deadp player) t)
    (setf (static-frame player) *skull-tile*)
    (begin-animation player (list *skull-tile*))))

(defmethod can-throw-p ((player player))
  (zerop (throw-timer player)))

(defmethod update-throw-timer ((player player))
  (with-slots (throw-timer) player
    (when (plusp throw-timer)
      (decf throw-timer))))

(defvar *player-1-joystick* 0)
(defvar *player-2-joystick* nil)

(defun arrow-direction ()
  (cond 
    ((holding-down-arrow-p) :down)
    ((holding-up-arrow-p) :up)
    ((holding-left-arrow-p) :left)
    ((holding-right-arrow-p) :right)))

(defparameter *player-1-config* 
  '(:throw 14 :deploy 15 :left 7 :right 5 :up 4 :down 6))

(defparameter *player-2-config*
  '(:throw 14 :deploy 15 :left 7 :right 5 :up 4 :down 6))

(defun joystick-button-p (keyword &key (joystick *player-1-joystick*) (config *player-1-config*))
  (when (numberp joystick)
    (let ((button (getf config keyword)))
      (if button
	  (joystick-button-pressed-p button joystick)))))

(defun keyboard-throw-p () (holding-shift-p))
(defun keyboard-deploy-p () (or (keyboard-down-p :space)
			       (holding-enter-p)))

(defmethod holding-throw-p ((player player)) nil)
(defmethod holding-deploy-p ((player player)) nil)
(defmethod holding-direction-p ((player player)) nil)
(defmethod held-direction ((player player)) nil)

(defmethod default-animation-frame ((player player)) 
  (with-slots (job sex) player
    (if (deadp player)
	*skull-tile*
	(first (character-animation-frames
		(walk-animation player)
		job sex)))))

(defmethod draw ((player player))
  (with-slots (x y) player
    (draw-frame
     (or (current-animation-frame player)
	 (default-animation-frame player))
     x y
     *character-cell-width*
     *character-cell-height*
     )))

(defmethod update :around ((player player))
  (unless (deadp player)
    (call-next-method)))

(defmethod update :before ((player player))
  (update-throw-timer player))

(defmethod walk-animation ((player player))
  (ecase (facing player)
    (:up :walk-up)
    (:down :walk-down)
    (:left :walk-left)
    (:right :walk-right)))

(defmethod attack-animation ((player player))
  (ecase (facing player)
    (:up :attack-up)
    (:down :attack-down)
    (:left :attack-left)
    (:right :attack-right)))

(defmethod cast-animation ((player player))
  (ecase (facing player)
    (:up :cast-up)
    (:down :cast-down)
    (:left :cast-left)
    (:right :cast-right)))

(defmethod walk-forward ((player player))
  (let ((anim (character-animation-frames
			   (walk-animation player)
			   (job player)
			   (sex player))))
    (begin-animation player (append anim anim))
    (slide-forward player 1)))

(defmethod walk-forward :around ((player player))
  (when (not (deadp player))
    (if (can-advance-p player)
	(call-next-method)
	(break-formation player))))

(defmethod think ((player player))
  (if (formation-p player)
      (when (followers player)
	(percent-of-time 1 (march player)))
      (think-freely player)))
	  
(defmethod think-freely ((player player))
  (if (facing-obstacle-p player)
      (punch-forward player)
      (progn (face player (direction-to-lord player))
	     (percent-of-time 2 (walk-forward player)))))

(defmethod begin-attack-animation ((player player))
  (begin-animation player
		   (character-animation-frames
		    (attack-animation player)
		    (job player)
		    (sex player))))

(defmethod punch-forward ((player player))
  (begin-attack-animation player)
  (dolist (thing (things-in-front-of player))
    (damage thing -20)))

(defmethod make-random-player ()
  (let ((job (random-choose *human-jobs*)))
    (make-instance 'player :sex (random-sex-for-job job) :job job)))

(defmethod facing-edge-p ((player player))
  (with-slots (grid-x grid-y) player
    (multiple-value-bind (new-x new-y)
	(step-in-direction grid-x grid-y (facing player))
      (not (valid-position-p new-x new-y)))))

;;; Formation positioning definitions

(defmethod in-front-of ((player player) &optional (distance 1))
  (with-slots (grid-x grid-y) player
    (step-in-direction grid-x grid-y (facing player) distance)))

(defmethod behind ((player player) &optional (distance 1))
  (with-slots (grid-x grid-y) player
    (step-in-direction grid-x grid-y (opposite-direction (facing player)) distance)))

(defmethod far-in-front-of ((player player)) (in-front-of player 2))
(defmethod far-behind ((player player)) (behind player 2))

(defmethod flanking-left ((player player) &optional (distance 1))
  (with-slots (grid-x grid-y) player
    (step-in-direction grid-x grid-y (leftward (facing player)) distance)))

(defmethod flanking-right ((player player) &optional (distance 1))
  (with-slots (grid-x grid-y) player
    (step-in-direction grid-x grid-y (rightward (facing player)) distance)))

(defmethod flanking-behind-left ((player player) &optional (distance 1))
  (with-slots (grid-x grid-y) player
    (step-in-direction grid-x grid-y (xelf::left-turn (leftward (facing player))) distance)))

(defmethod flanking-behind-right ((player player) &optional (distance 1))
  (with-slots (grid-x grid-y) player
    (step-in-direction grid-x grid-y (xelf::right-turn (rightward (facing player))) distance)))

(defmethod flanking-far-left ((player player)) (flanking-left player 2))
(defmethod flanking-far-right ((player player)) (flanking-right player 2))
(defmethod flanking-behind-far-left ((player player)) (flanking-behind-left player 2))
(defmethod flanking-behind-far-right ((player player)) (flanking-behind-right player 2))

(defun things-at (gx gy)
  (let (things)
    (do-nodes (node (current-buffer))
      (when (typep node (find-class 'thing))
	(with-slots (grid-x grid-y) node
	  (when (and (= gx grid-x)
		     (= gy grid-y))
	    (push node things)))))
    things))

(defun player-at (gx gy)
  (remove-if-not #'(lambda (x)
		     (typep x (find-class 'player)))
		 (things-at gx gy)))
  
(defmethod player-in-front-of ((player player) &optional (distance 1))
  (multiple-value-call #'player-at (in-front-of player distance)))

(defmethod things-in-front-of ((player player) &optional (distance 1))
  (multiple-value-call #'things-at (in-front-of player distance)))

(defmethod player-behind ((player player) &optional (distance 1))
  (multiple-value-call #'player-at (behind player distance)))

(defmethod facing-obstacle-p ((player player))
  (multiple-value-bind (gx gy) (in-front-of player)
    (some #'obstacle-p (things-at gx gy))))

(defmethod can-advance-p ((player player))
  (or (and (not (minusp (grid-x player)))
	   (minusp (grid-y player)))
      (and (not (facing-edge-p player))
	   (not (facing-obstacle-p player)))))

(defmethod can-advance-p :around ((player player))
  (if (formation-p player)
      (call-next-method)
      (and (not (facing-edge-p player))
	   (null (things-in-front-of player)))))

(defmethod lead-formation ((player player) followers)
  (setf (formation-p player) t)
  (setf (followers player) followers))

(defparameter *formation-positions*
  '(:in-front-of :behind :far-in-front-of :far-behind
    :flanking-left :flanking-right
    :flanking-far-left :flanking-far-right
    :flanking-behind-left :flanking-behind-right
    :flanking-behind-far-left :flanking-behind-far-right))

(defun formation-position-function (position)
  (assert (member position *formation-positions*))
  (symbol-function (intern (symbol-name position) (find-package :wizl0rd))))

(defmethod formation-position ((player player) position)
  ;;(assert (formation-p player))
  (funcall (formation-position-function position) player))

(defmethod join-formation ((player player) leader position)
  (setf (formation-p player) t)
  (setf (leader player) leader)
  (face player (facing leader))
  (multiple-value-call #'move-to-grid player
    (formation-position leader position)))

(defmethod formation-members ((player player))
  (when (formation-p player)
    (if (followers player)
	(cons player (followers player))
	(formation-members (leader player)))))

(defmethod march ((player player))
  (map nil #'walk-forward (formation-members player)))

(defmethod break-formation ((player player))
  (when (formation-p player)
    (if (formation-follower-p player)
	(progn
	  (break-formation (leader player))
	  (setf (leader player) nil)
	  (setf (formation-p player) nil))
	(let ((f (followers player)))
	  (setf (formation-p player) nil)
	  (setf (followers player) nil)
	  (mapc #'break-formation f)))))

(defparameter *formations*
  '((warrior :flanking-right warrior :flanking-left warrior :far-behind wizard)
    (warrior :behind wizard :flanking-left warrior)
    (dark-knight :behind cultist :far-behind cultist :flanking-behind-left cultist)
    (warrior :flanking-behind-left warrior :behind healer :far-behind wizard)
    (berserker :flanking-behind-left berserker :flanking-behind-right berserker)
    (captain :flanking-behind-left pirate :flanking-right pirate :flanking-left merchant)
    (ninja :behind bunny :flanking-right bunny :flanking-left ranger)
    (soldier :flanking-left soldier :flanking-right soldier :behind dark-knight :far-behind healer)))

(defclass lord (player)
  ((job :initform :lord)
   (sex :initform :male)
   (obstacle-p :initform t)
   (slide-frames :initform 100)))

(defun find-lord ()
  (first (find-instances (current-buffer) 'lord)))

(defun lord-y ()
  (let ((lord (find-lord)))
    (when lord (grid-y lord))))

(defun lord-x ()
  (let ((lord (find-lord)))
    (when lord (grid-x lord))))

(defmethod think ((lord lord))
  (if (can-advance-p lord)
      (walk-forward lord)
      (turn-around lord)))

(defmethod direction-to-lord ((thing thing))
  (cond ((< (grid-y thing) (lord-y)) :down)
	((= (grid-y thing) (lord-y))
	 (if (< (grid-x thing) (lord-x))
	     :right
	     :left))
	(t :down)))

(defmethod direction-to-knight ((thing thing))
  (find-direction (grid-x thing) (grid-y thing)
		  (grid-x (knight)) (grid-y (knight))))

(defun random-formation () (random-choose *formations*))

(defmethod make-follower ((leader player) job position)
  (let ((follower (make-instance 'player
			       :sex (random-sex-for-job job)
			       :job job)))
    (add-node (current-buffer) follower)
    (join-formation follower leader position)
    follower))

(defmethod make-leader (job gx gy &optional (facing :down))
  (let ((leader (make-instance 'player :job job :sex (random-sex-for-job job))))
    (add-node (current-buffer) leader)
    (move-to-grid leader gx gy)
    leader))

(defun make-formation (formation x y)
  (destructuring-bind (leader-job &rest pairs)
      (mapcar #'make-keyword formation)
    (let ((leader (make-leader leader-job x y))
	  (followers nil))
      (loop while pairs do
	(let ((position (pop pairs))
	      (job (pop pairs)))
	  (push (make-follower leader job position) followers)))
      (assert (not (null followers)))
      (lead-formation leader followers)
      (cons leader followers)))) 

;;; The spinning magic boomerang axe

(defparameter *throw-disabled-time* 15)
(defparameter *auto-return-distance* 350)
(defparameter *axe-movement-frames* 15)
(defparameter *axe-images* (image-set "axe" 5))

(defclass axe (thing)
  ((image :initform "axe.png")
   (slide-frames :initform 6)
   (phase :initform 30)
   (last-contact :initform nil :accessor last-contact)
   (contact-timer :initform 0 :accessor contact-timer)
   (timer :initform 6)))

(defmethod collide :around ((thing thing) (axe axe))
  (when (or (null (last-contact axe))
	    (not (xelf::object-eq thing (last-contact axe))))
    (call-next-method))
  (setf (contact-timer axe) 20)
  (setf (last-contact axe) thing))

(defmethod collide ((player player) (axe axe))
  (damage player -40)
  (bounce axe))

(defmethod bounce ((axe axe))
  (with-slots (direction) axe
    (when (sliding-p axe)
      (cancel-slide axe)
      (restore-from-slide axe))
    (setf (slot-value axe 'timer) 0)
    (face axe (opposite-direction direction))))

(defmethod update :after ((axe axe))
  (when (plusp (contact-timer axe))
    (decf (contact-timer axe)))
  (when (zerop (contact-timer axe))
    (setf (last-contact axe) nil)))

(defparameter *phase* 60)

(defmethod update ((axe axe))
  (with-slots (timer phase image) axe
    (when (plusp timer) 
      (decf timer))
    (when (plusp phase)
      (decf phase))
    (unless (plusp phase)
      (setf phase *phase*))
    (setf image (random-choose *axe-images*)))
  (when (not (sliding-p axe))
    (slide-forward axe)))
    
(defmethod arrive ((axe axe))
  (when (not (sliding-p axe))
    (snap-to-grid axe)
    (if (> (distance-between (knight)
			     axe)
	   *auto-return-distance*)
	(bounce axe)
	(slide-forward axe))))

(defmethod draw ((axe axe))
  (with-slots (width height image phase) axe
    (multiple-value-bind (x y) (location axe)
      (draw-textured-rectangle-* x y 0 width height
				 (find-texture image)
				 :angle (* 6 phase)))))

;;; The Knight

(defclass knight (player)
  ((job :initform :warrior)
   (sex :initform :male)
   (slide-frames :initform 17)
   (player-id :initform 1)))

(defun knight ()
  (first (find-instances (current-buffer) 'knight)))

(defmethod think ((knight knight))
  (when (holding-direction-p knight)
    (face knight (held-direction knight))
    (walk-forward knight)))

(defun find-axe ()
  (first (find-instances (current-buffer) 'axe)))

(defmethod make-axe ((knight knight))
  (make-instance 'axe))

(defmethod throw-axe ((knight knight))
  (unless (find-axe)
    (with-slots (throw-timer paint-color grid-x grid-y direction) knight
      (setf throw-timer 20)
      (let ((axe (make-axe knight)))
	(add-node (current-buffer) axe)
	(move-to-grid axe grid-x grid-y)
	(face axe direction)))))

(defmethod grab ((knight knight) (axe axe))
  (setf (throw-timer knight) 20)
  (destroy axe))

(defmethod collide :after ((knight knight) (axe axe))
  (with-slots (timer) axe
    (when (zerop timer)
      (grab knight axe))))

(defmethod collide ((knight knight) (axe axe)) nil)

(defmethod holding-fire-p ((knight knight)) (holding-shift-p))
(defmethod holding-deploy-p ((knight knight)) (holding-enter-p))
(defmethod holding-direction-p ((knight knight)) (arrow-direction))
(defmethod held-direction ((knight knight)) (arrow-direction))

(defmethod update :before ((knight knight))
  (when (holding-fire-p knight)
    (hide-terminal)
    (throw-axe knight)))

;;; Walls

(defclass wall (thing)
  ((orientation :initform :horizontal :accessor orientation :initarg :orientation)
   (defense :initform 2)
   (obstacle-p :initform t)))

(defmethod collide ((wall wall) (axe axe))
  (damage wall -50)
  (bounce axe))

(defmethod initialize-instance :after ((wall wall) &key)
  (with-slots (orientation static-frame) wall
    (setf static-frame
	  (ecase orientation
	    (:horizontal (random-choose (list *wall-right* *wall-left*)))
	    (:vertical (random-choose (list *wall-up* *wall-down*)))
	    (:tower *tower-tile*)))))

(defclass tower (wall)
  ((defense :initform 5)))

(defmethod initialize-instance :after ((tower tower) &key)
  (with-slots (orientation static-frame) tower
    (setf static-frame
	  (ecase orientation
	    (:horizontal *wall-tower-right*)
	    (:vertical *wall-tower-up*)))))

;;; The arena

(defclass arena (xelf:buffer)
  ((quadtree-depth :initform 9)))

(defmethod draw-object-layer ((arena arena))
  (let ((vec (make-array 1 :adjustable t :fill-pointer 0)))
    (with-slots (objects) arena
      (loop for object being the hash-values of objects do
	(vector-push-extend object vec))
      ;; draw from top to bottom due to jrpg character overspill 
      (setf vec (sort vec #'< :key #'(lambda (p)
				       (or (slot-value p 'y) 0))))
      (loop for object across vec do (draw object)))))

(defmethod grid-offset ((arena arena)) *margin-width*)

(defmethod grid-object-size ((arena arena))
  (- (units 1) (* 2 *margin-width*)))

(defun drop-wall (gx gy &optional (orientation :horizontal))
  (let ((wall (make-instance 'wall :orientation orientation)))
    (add-node (current-buffer) wall)
    (move-to-grid wall gx gy)))

(defun draw-horizontal-wall (gx gy length)
  (dotimes (n length)
    (drop-wall (+ n gx) gy)))

(defun draw-vertical-wall (gx gy length)
  (dotimes (n length)
    (drop-wall gx (+ n gy))))

(defun draw-border-wall (gx gy width height)
  (draw-horizontal-wall 0 0 width)
  (draw-horizontal-wall 0 (- height 1) width)
  (draw-vertical-wall 0 1 (- height 2))
  (draw-vertical-wall (- width 1) 1 (- height 2)))

(defun draw-courtyard (gx gy width height &optional shaft)
  (let* ((lenx (truncate (/ width 2)))
	 (leny (truncate (/ height 2)))
	 (cx (+ gx lenx))
	 (cy (+ gy leny)))
    (draw-horizontal-wall gx gy (- cx gx 1))
    (draw-horizontal-wall cx gy (- cx gx -1))
    (when shaft
      (draw-vertical-wall (- cx 2) (+ 1 gy) 3)
      (draw-vertical-wall cx (+ 1 gy) 3))
    (percent-of-time (by-level 0 10 20 30 40)
      (place (random-enemy-not-tracer) (+ gx 5) (+ gy 3)))
    (place (make-instance 'wall) (- cx 2) (+ gy height -2))
    (draw-horizontal-wall gx (+ gy height -1) width)
    (draw-vertical-wall gx (1+ gy) (- height 2))
    (draw-vertical-wall (+ -1 gx width) (1+ gy) (- height 2))))

(defun turnstile (gx gy) 
  (draw-horizontal-wall (+ gx 1) (+ gy 3) 3)
  (draw-horizontal-wall (+ gx 3) (+ gy 3) 3)
  (draw-vertical-wall (+ gx 3) (+ gy 1) 2)
  (draw-vertical-wall (+ gx 3) (+ gy 4) 2))
  
(defmethod quit-game ((arena arena))
  (quit))

(defun width-in-units () (truncate (/ *width* *unit*)))
(defun height-in-units () (truncate (/ *height* *unit*)))

(defmethod draw :before ((arena arena))
  (draw-box 0 0 *width* *height* :color "gray20")
  (draw-image "arena.png" 0 0))

(defmethod reset-game ((arena arena))
  (stop arena)
  (at-next-update (switch-to-buffer (make-instance 'arena)))
  (at-next-update (destroy arena)))

(defmethod initialize-instance :after ((arena arena) &key)
  (with-buffer arena
    (resize arena *width* *height*)
    ;;(draw-border-wall 0 0 (width-in-units) (height-in-units))
    (bind-event arena '(:r :control) 'reset-game)
    (let ((knight (make-instance 'knight)))
      (add-node (current-buffer) knight)
      (move-to-grid knight 2 16))
    (let ((lord (make-instance 'lord)))
      (add-node (current-buffer) lord)
      (face lord :left)
      (move-to-grid lord 21 20))
    (draw-horizontal-wall 0 19 (width-in-units))
    (draw-horizontal-wall 0 21 (width-in-units))
    (drop-wall 1 19 :tower)
    (drop-wall 38 19 :tower)))

(defun drop-formation ()
  (make-formation (random-formation) (+ 5 (random 25)) -1))
  
(defparameter *formation-timer* 0)
(defparameter *formation-period* (seconds 10))
(defun reset-formation-timer ()
  (setf *formation-timer* *formation-period*))
(defun update-formation-timer ()
  (decf *formation-timer*)
  (unless (plusp *formation-timer*)
    (drop-formation)
    (reset-formation-timer)))

(defmethod update :after ((arena arena))
  (update-formation-timer)
  (update-terminal-timer))

;;; Main program

(defparameter *title-string* "wizl0rd v0.1a (pre-alpha)")

(defun wizl0rd ()
  (setf *fullscreen* nil)
  (setf *font-texture-scale* 2)
  (setf *font-texture-filter* :linear)
  (setf *window-title* *title-string*)
  (setf *screen-width* *width*)
  (setf *screen-height* *height*)
  (setf *nominal-screen-width* *width*)
  (setf *nominal-screen-height* *height*)
  (setf *scale-output-to-window* t) 
  (setf *default-texture-filter* :nearest)
  (setf *use-antialiased-text* nil)
  (setf *inhibit-splash-screen* nil)
  (setf *frame-rate* 60)
  (disable-key-repeat) 
  (with-session 
    (open-project "wizl0rd")
    (index-all-images)
    (index-all-samples)
    (index-pending-resources)
    (preload-resources)
    (setf xelf:*terminal-bottom* (- *height* (units 1.5)))
    (setf xelf:*prompt-font* xelf:*terminal-font*)
    (setf xelf:*terminal-left* (units 3.4))
    (setf xelf:*terminal-display-lines* 50)
    (dolist (line (split-string-on-lines *wizl0rd-copyright-notice*))
      (logging line))
    (show-terminal)
    (at-next-update (switch-to-buffer (make-instance 'arena)))))


    ;; (bind-event arena '(:escape) 'setup)
    ;; (bind-event arena '(:y :control) 'retry-level)
    ;; (bind-event arena '(:p :control) 'pause)
    ;; (bind-event arena '(:n :control) 'next-level)
    ;; (bind-event arena '(:q :control) 'quit-game)
    ;; (place (make-instance 'wall) (+ 6 (random 25)) 1)
    ;; (place (make-instance 'wall) (+ 12 (random 25)) 40)
    ;; (place (make-instance 'wall) (+ 40 (random 13)) 40)
    ;; (set-player-1 (make-instance 'player-1))
    ;; (when (coop-p)
    ;;   (set-player-2 (make-instance 'player-2)))
    ;; (let ((template (make-level-template)))
    ;;   (expand-entries 
    ;;    (transform-entries 
    ;; 	(template-entries template)
    ;; 	4 4)))
    ;; (do-nodes (node arena)
    ;;   (spawn node))))
    



;;; wizl0rd.lisp ends here

